var photoForm = document.getElementById('photo-form');
var photoTitle = document.getElementById('photo-title')
var photoSelect = document.getElementById('photo-file');
var uploadButton = document.getElementById('photo-submit');

var feedItemTemplate = document.getElementById('feedItem');
var feed = document.getElementById('feed');
var channelList = document.getElementById('channelList');

var signupForm = document.getElementById('signup-form');
var loginForm = document.getElementById('login-form');

function signup(username, password) {
	return new Promise(function(resolve, reject) {
		if (!username) {
			reject(Error('username is required'));
			return;
		}
		if (!password) {
			reject(Error('password is required'));
			return;
		}

		var xhr = new XMLHttpRequest();
		xhr.open('POST', '/signup', true);
		xhr.onload = function() {
			if (xhr.status === 200) {
				// If successful, resolve the promise by passing back the request response
				resolve(JSON.parse(xhr.responseText));
			} else {
				// If it fails, reject the promise with a error message
				reject(Error("signup failed; error code:" + xhr.statusText));
			}
		};
		xhr.onerror = function() {
			reject(Error('There was a network error.'));
		};
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.send(JSON.stringify({
			username: username,
			password: password
		}, undefined, 2));
	});
}
function login(username, password) {
	return new Promise(function(resolve, reject) {
		if (!username) {
			reject(Error('username is required'));
			return;
		}
		if (!password) {
			reject(Error('password is required'));
			return;
		}

		var xhr = new XMLHttpRequest();
		xhr.open('POST', '/login', true);
		xhr.onload = function() {
			if (xhr.status === 200) {
				// If successful, resolve the promise by passing back the request response
				resolve(JSON.parse(xhr.responseText));
			} else {
				// If it fails, reject the promise with a error message
				reject(Error("login failed; error code:" + xhr.statusText));
			}
		};
		xhr.onerror = function() {
			reject(Error('There was a network error.'));
		};
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.send(JSON.stringify({
			username: username,
			password: password
		}, undefined, 2));
	});
}

function uploadFile(file) {
	return new Promise(function(resolve, reject) {
		var formData = new FormData();
		formData.append('file', file, file.name);
	
		var xhr = new XMLHttpRequest();
		xhr.open('POST', '/upload', true);
		xhr.onload = function() {
			if (xhr.status === 200) {
				// If successful, resolve the promise by passing back the request response
				resolve(JSON.parse(xhr.responseText));
			} else {
				// If it fails, reject the promise with a error message
				reject(Error("feed item creation failed; error code:" + xhr.statusText));
			}
		};
		xhr.onerror = function() {
			reject(Error('There was a network error.'));
		};
		xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'));
		xhr.send(formData);
	});
}

function postPhoto(item) {
	return new Promise(function(resolve, reject) {
		if (!item.upload_id) {
			reject(Error('item.upload_id is required'));
			return;
		}
		if (!item.channel) {
			reject(Error('item.channel is required'));
			return;
		}

		var xhr = new XMLHttpRequest();
		xhr.open('POST', '/photos', true);
		xhr.onload = function() {
			if (xhr.status === 200) {
				// If successful, resolve the promise by passing back the request response
				resolve(JSON.parse(xhr.responseText));
			} else {
				// If it fails, reject the promise with a error message
				reject(Error("feed item creation failed; error code:" + xhr.statusText));
			}
		};
		xhr.onerror = function() {
			reject(Error('There was a network error.'));
		};
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'));
		xhr.send(JSON.stringify(item, undefined, 2));
	});
}

function subscribe(channel) {
	return new Promise(function(resolve, reject) {
		var xhr = new XMLHttpRequest();
		xhr.open('PUT', '/subscriptions/' + channel, true);
		xhr.onload = function() {
			if (xhr.status === 200) {
				// If successful, resolve the promise by passing back the request response
				resolve(JSON.parse(xhr.responseText));
			} else {
				// If it fails, reject the promise with a error message
				reject(Error("subscription failed; error code:" + xhr.statusText));
			}
		};
		xhr.onerror = function() {
			reject(Error('There was a network error.'));
		};
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'));
		xhr.send(null);
	});
}

function unsubscribe(channel) {
	return new Promise(function(resolve, reject) {
		var xhr = new XMLHttpRequest();
		xhr.open('DELETE', '/subscriptions/' + channel, true);
		xhr.onload = function() {
			if (xhr.status === 200) {
				// If successful, resolve the promise by passing back the request response
				resolve(JSON.parse(xhr.responseText));
			} else {
				// If it fails, reject the promise with a error message
				reject(Error("subscription failed; error code:" + xhr.statusText));
			}
		};
		xhr.onerror = function() {
			reject(Error('There was a network error.'));
		};
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'));
		xhr.send(null);
	});
}

function myFeed() {
	return new Promise(function(resolve, reject) {
		var xhr = new XMLHttpRequest();
		xhr.open('GET', '/feed', true);
		xhr.onload = function() {
			if (xhr.status === 200) {
				// If successful, resolve the promise by passing back the request response
				resolve(JSON.parse(xhr.responseText));
			} else {
				// If it fails, reject the promise with a error message
				reject(Error("feed fetch failed; error code:" + xhr.statusText));
			}
		};
		xhr.onerror = function() {
			reject(Error('There was a network error.'));
		};
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'));
		xhr.send(null);
	});
}

function refresh() {
	var template = _.template(feedItemTemplate.innerHTML);
	return myFeed().then(function(response) {
		feed.innerHTML = response.channels.map(template).join('');
		channelList.innerHTML = response.channels.map(function(item) {
			return '<li>' + item.channel +
				' <button data-channel=' + item.channel + '>&times</button>' +
				"</li>";
		}).join('');
	});
}

photoForm.onsubmit = function(evt) {
	evt.preventDefault();
	var files = photoSelect.files,
		file = files[0];
	if (0 === files.length) {
		alert("no files selected");
		return;
	}
	if (!file.type.match('image.*')) {
		alert("please select an image file");
		return;
	}

	uploadFile(file).then(function(response) {
		postPhoto({
			upload_id: response._id,
			channel: document.getElementById('photo-channel').value,
			title: 'some photo title'
		}).then(function(response) {
			console.log(response);
			init();
		});
	}).catch(function(err) {
		localStorage.removeItem('token');
		init();
	});
};

function init() {
	if (localStorage.getItem('token')) {
		photoForm.style.display = "";
		signupForm.style.display = "none";
		loginForm.style.display = "none";
		refresh();
	} else {
		photoForm.style.display = "none";
		signupForm.style.display = "";
		loginForm.style.display = "";
	}
}

signupForm.onsubmit = function(evt) {
	evt.preventDefault();
	signup(document.getElementById('signup-username').value,
	       document.getElementById('signup-password').value)
	.then(function(response) {
		console.log('signup response:', response);
		if (response.token) {
			localStorage.setItem('token', response.token);
		}
		init();
	});
};

loginForm.onsubmit = function(evt) {
	evt.preventDefault();
	login(document.getElementById('login-username').value,
	      document.getElementById('login-password').value)
	.then(function(response) {
		console.log('login response:', response);
		if (response.token) {
			localStorage.setItem('token', response.token);
		}
		init();
	});
};

init();
document.getElementById('channelList').addEventListener('click', function(evt) {
	evt.preventDefault();
	var channelName = evt.target.dataset.channel;
	unsubscribe(channelName).then(refresh);
}, false);
