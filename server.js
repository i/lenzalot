#!/bin/env node
/// <reference path="typings/node/node.d.ts"/>
/// <reference path="typings/express/express.d.ts"/>
/// <reference path="typings/mongoose/mongoose.d.ts"/>
var fs = require('fs');
var _ = require('underscore');
var mongoose = require('mongoose');
var express = require('express');
var parted = require('parted');
var morgan = require('morgan');
var bcrypt = require('bcrypt');

var Schema = mongoose.Schema;
var SALT_WORK_FACTOR = 10;

var publicPath = __dirname + '/public';
// middlewares
var app = express();
app.use(morgan('combined'));
app.use(express.static(publicPath));
app.use(function(req, res, next) { // check authorization
	var token = req.headers.authorization;
	if (!token) {
		next();
		return;
	}
	token = token.replace(/bearer/i, '').trim();
	if (!token) {
		next();
		return;
	}
	req.token = token;
	next();
});
var partedConfig = {
  // custom file path
  path: (process.env.OPENSHIFT_DATA_DIR || __dirname + '/tmp_uploads').replace(/\/+$/,''),
  // memory usage limit per request
  limit: 30 * 1024,
  // disk usage limit per request
  diskLimit: 30 * 1024 * 1024,
  // enable streaming for json/qs
  stream: true
};
app.use(parted(partedConfig));

var mmm = require('mmmagic'),
	Magic = mmm.Magic;

var magic = new Magic(mmm.MAGIC_MIME_TYPE);


function authRequired(req, res, next) {
	if (!req.token) {
		res.status(403).send({error: {message: 'authentication token required'}});
		return;
	}
	Token.findById(req.token, function(err, user) {
		if (err) {
			console.error(err);
			res.status(403).send({ error: err });
			return;
		}
		if (!user) {
			err = {message: 'token not found'};
			console.error(err);
			res.status(403).send({ error: err });
			return;
		}
		User.findById(user.user_id, function(err, user) {
			if (err) {
				console.error(err);
				res.status(403).send({ error: err });
				return;
			}
			if (!user) {
				err = {message: 'user not found'};
				console.error(err);
				res.status(403).send({ error: err });
				return;
			}
			req.user = user;
			next();
		});
	});
}

// models
var UserSchema = new Schema({
    username: { type: String, required: true, index: { unique: true } },
    password: { type: String, required: true },
	subscriptions: { type: [String], index: true }
});
UserSchema.pre('save', function(next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});
UserSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};
var User = mongoose.model('User', UserSchema);
var Token = mongoose.model('Token', {
	user_id: { type: String, required: true, index: true },
	created_at: { type: Date, default: Date.now }
});

var Upload = mongoose.model('Upload', {
	filename: { type: String, required: true },
	temppath: { type: String, required: true },
	filetype: { type: String, default: '' },
	user_id: { type: String, required: true },
	created_at: { type: Date, default: Date.now }
});

var Photo = mongoose.model('Photo', {
	// TODO: photo focus coordinates
	channel: { type: String, required: true, index: true },
	url: { type: String, required: true },
	created_at: { type: Date, default: Date.now },
	title: { type: String, required: false, default: '' },
	mime: { type: String, default: '' }
});

// routes
app.post('/upload', authRequired, function(req, res) {
	// TODO: setup `tmpwatch`
	var f = req.files.file,
		fname = f.name,
	    upload = new Upload({
			filename: fname,
			temppath: f.path,
			user_id: req.user.id
		});

	upload.save(function(err, upload) {
		if (err) {
			console.error(err);
			res.send({ error: err });
			return;
		}
		var destination = partedConfig.path + '/' + upload.id;

		upload.temppath = destination;
		fs.rename(f.path, destination, function(err) {
			if (err) {
				console.error(err);
				res.send({ error: err });
				return;
			}

			magic.detectFile(destination, function(err, filetype) {
				if (err) {
					console.error(err);
					res.status(400).send({ error: err });
					return;
				}
				upload.filetype = filetype;

				upload.save(function(err, upload) {
					if (err) {
						console.error(err);
						res.status(400).send({ error: err });
						return;
					}
					res.send({status: 'ok', _id: upload.id});
				});
			});
		});
	});
});

app.post('/signup', function(req, res) {
	var username = req.body.username;
	if (!(/^[a-zA-Z]+[a-zA-Z0-9]+$/.test(username))) {
		var err = {message: 'wrong username, choose another one'};
		console.error(err);
		res.send({ error: err });
		return;
	}
	var user = new User({
		username: username,
		password: req.body.password
	});
	User.findOne({ username: username }, function(err, userFound) {
		if (err) {
			console.error(err);
			res.send({ error: err });
			return;
		}
		if (userFound) {
			err = {message: "username is already taken"};
			console.error(err);
			res.send({ error: err });
			return;
		}

		user.save(function(err, user) {
			var token = new Token({user_id: user.id});
			token.save(function(err, token) {
				if (err) {
					console.error(err);
					res.send({ error: err });
					return;
				}
				res.send({token: token.id});
			});
		});
	});
});

app.post('/login', function(req, res) {
	User.findOne({ username: req.body.username }, function(err, user) {
		if (err) {
			console.error(err);
			res.status(401).send({ error: err });
			return;
		}
		if (!user) {
			err = {message: 'user not found'};
			console.error(err);
			res.status(401).send({ error: err });
			return;
		}

		user.comparePassword(req.body.password, function(err, isMatch) {
			if (err) {
				console.error(err);
				res.send({ error: err });
				return;
			}
			if (!isMatch) {
				err = {message: "password didn't match"};
				console.error(err);
				res.status(401).send({ error: err });
				return;
			}
			var token = new Token({user_id: user.id});
			token.save(function(err, token) {
				if (err) {
					console.error(err);
					res.send({ error: err });
					return;
				}
				res.send({token: token.id});
			});
		});
	});
});

app.post('/photos', authRequired, function(req, res) {
	Upload.findById(req.body.upload_id, function(err, upload) {
		if (err) {
			console.error(err);
			res.status(400).send({ error: err });
			return;
		}
		if (!upload) {
			err = {message: "didn't find any uploads with specified id"};
			console.error(err);
			res.status(404).send({ error: err });
			return;
		}
		if (!(/image\/.+/.test(upload.filetype))) {
			err = {message: "upload is not an image"};
			console.error(err);
			res.status(400).send({ error: err });
			return;
		}

		var destination = publicPath + '/uploads/' + upload.id + '.jpg';
		fs.rename(upload.temppath, destination, function(err) {
			if (err) {
				console.error(err);
				res.status(400).send({ error: err });
				return;
			}
			var photo = new Photo({
				url: '/uploads/' + upload.id + '.jpg',
				channel: req.user.username + '/' + req.body.channel,
				title: req.body.title,
				mime: upload.filetype
			});
			photo.save(function(err, photo) {
				if (err) {
					console.error(err);
					res.status(400).send({ error: err });
					return;
				}

				res.send(_(photo.toJSON()).omit('__v'));
			});
		});
	});
});

app.get('/search', function(req, res) {
	User.aggregate([
		{ $unwind : "$subscriptions" },
		{
			"$group": {
				_id: "$subscriptions",
				rating: { "$sum": 1 }
			}
		},
		{ "$sort": { "rating": -1 } },
        { "$limit": 10 }
	], function(err, subscriptions) {
		if (err) {
			console.error(err);
			res.status(400).send({ error: err });
			return;
		}
		res.send({
			channels: subscriptions.map(function(item) {
				return {
					channel: item._id,
					rating: item.rating
				};
			})
		});
	});
});

app.put('/subscriptions/:username/:channel', authRequired, function(req, res) {
	var channel = req.params.username + '/' + req.params.channel;
	Photo.findOne({channel: channel}, function(err, photo) {
		if (err) {
			console.error(err);
			res.status(400).send({ error: err });
			return;
		}
		if (!photo) {
			err = {message: 'channel not found'};
			console.error(err);
			res.status(404).send({ error: err });
			return;
		}
		User.findByIdAndUpdate(req.user.id, {"$addToSet": { "subscriptions": channel }}, function(err, user) {
			if (err) {
				console.error(err);
				res.status(400).send({ error: err });
				return;
			}
			res.send({subscriptions: user.subscriptions});
		});
	});
});

app.delete('/subscriptions/:username/:channel', authRequired, function(req, res) {
	var channel = req.params.username + '/' + req.params.channel;
	User.findByIdAndUpdate(req.user.id, {"$pull": { "subscriptions": channel }}, function(err, user) {
		if (err) {
			console.error(err);
			res.status(400).send({ error: err });
			return;
		}
		res.send({subscriptions: user.subscriptions});
	});
});

app.get('/feed', authRequired, function(req, res) {
	Photo.aggregate([
		{"$match": {channel: {"$in": req.user.subscriptions}}},
		{"$sort": {created_at: -1}}
		// ,
		// {"$group": {
		// 	_id: "$channel",
		// 	latest_photo: { "$first": "$$ROOT" }
		// }}
	], function(err, channels) {
		if (err) {
			console.error(err);
			res.status(400).send({ error: err });
			return;
		}
		if (!channels) {
			res.send({channels: []});
			return;
		}
		res.send({channels: channels});
		// res.send({channels: channels.map(function(channel) {
		// 	var p = channel.latest_photo;
		// 	return {
		// 		channel: channel._id,
		// 		latest_photo: {
		// 			url: p.url,
		// 			title: p.title,
		// 			created_at: p.created_at
		// 		}
		// 	};
		// })});
	});
});

app.get('/me', authRequired, function(req, res) {
	res.send({ user: req.user });
});
app.get('/feed/:username/:channel', function(req, res) {
	User.findOne({username: req.params.username}, function(err, user) {
		if (err) {
			console.error(err);
			res.status(400).send({ error: err });
			return;
		}
		if (!user) {
			err = {message: "user not found"};
			console.error(err);
			res.status(404).send({ error: err });
			return;
		}
		var channel = req.params.username + '/' + req.params.channel;
		Photo.find({channel: channel}, function(err, photos) {
			if (err) {
				console.error(err);
				res.send({ error: err });
				return;
			}
			if (!photos.length) {
				err = {message: "no photos out there"};
				console.error(err);
				res.status(404).send({ error: err });
				return;
			}

			res.send({
				photos: photos.map(function(photo) {
					return { url: photo.url, title: photo.title };
				})
			});
		});
	});
});

var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080;
var ipaddr = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";

// init
// default to a 'localhost' configuration:
var mongoConnectionStr = '127.0.0.1:27017/lenzalot';
// if OPENSHIFT env variables are present, use the available connection info:
if (process.env.OPENSHIFT_MONGODB_DB_PASSWORD) {
  mongoConnectionStr = process.env.OPENSHIFT_MONGODB_DB_USERNAME + ":" +
	process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" +
	process.env.OPENSHIFT_MONGODB_DB_HOST + ':' +
	process.env.OPENSHIFT_MONGODB_DB_PORT + '/' +
	process.env.OPENSHIFT_APP_NAME;
}
mongoose.connect(mongoConnectionStr);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
var server;
db.once('open', function(callback) {
	server = app.listen(port, ipaddr, function() {
		console.log('listening at port %s', server.address().port);
	});
});

